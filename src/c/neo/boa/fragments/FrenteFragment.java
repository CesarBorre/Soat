package c.neo.boa.fragments;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import c.neo.boa.DataActivity;
import c.neo.boa.R;
import c.neo.boa.utils.DateUtils;

public class FrenteFragment extends Fragment{

	Context context;
	
	TextView fechaVen;
	TextView poliza;
	ImageView img;
	RelativeLayout layOutValid;
	String fechaV="";
	
	int iConta = 0;
	
	private static final String SOAP_ACTION = "";
	private static final String METHOD_NAME = "insertDate";
	private static final String NAMESPACE = "http://insertAuto.neo.c";
	private static final String URL = "http://148.245.107.245:8095/InsertDateAuto/services/ServiceAuto?wsdl";

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){

		final View rootView = inflater.inflate(R.layout.fragment_frente,
				container, false);
		context = (Context) getArguments().get("");
		
		ViewHolder viewHolder = new ViewHolder();

		viewHolder.contenedor = (LinearLayout)rootView.findViewById(R.id.contenedorPrincipal);
		viewHolder.foto = (ImageView)rootView.findViewById(R.id.foto_img);
		viewHolder.cedulaLbl = (TextView)rootView.findViewById(R.id.lblCedulaID);
		viewHolder.cedulaTxt = (TextView)rootView.findViewById(R.id.txtCedulaID);
		viewHolder.nombreLbl = (TextView)rootView.findViewById(R.id.label_nombre);
		viewHolder.nombreTxt = (TextView)rootView.findViewById(R.id.nombre);
		viewHolder.apellidosLbl = (TextView)rootView.findViewById(R.id.label_apellidos);
		viewHolder.apellidosTxt = (TextView)rootView.findViewById(R.id.apellidos);
		viewHolder.profesionLbl = (TextView)rootView.findViewById(R.id.label_profesion);
		viewHolder.profesionTxt = (TextView)rootView.findViewById(R.id.profesion);
		viewHolder.estadoCivilLbl = (TextView)rootView.findViewById(R.id.label_estado_civil);
		viewHolder.estadoCivilTxt = (TextView)rootView.findViewById(R.id.estado_civil);
		viewHolder.fechaNacLbl = (TextView)rootView.findViewById(R.id.label_fecha_nacimiento);
		viewHolder.fechaNacTxt = (TextView)rootView.findViewById(R.id.fecha_nacimiento);
		viewHolder.lugarNacLbl = (TextView)rootView.findViewById(R.id.label_lugar_nacimiento);
		viewHolder.lugarNacTxt = (TextView)rootView.findViewById(R.id.lugar_nacimiento);
		viewHolder.lblMatricula = (TextView)rootView.findViewById(R.id.lblMatricula);
		viewHolder.tipoAutoTxt = (TextView)rootView.findViewById(R.id.tipoAuto);
		viewHolder.fotoMini = (ImageView)rootView.findViewById(R.id.imgFantasmaID);
		viewHolder.pay = (Button)rootView.findViewById(R.id.buyWallet);

		rootView.setTag(viewHolder);
		
		fechaVen = (TextView)rootView.findViewById(R.id.fecha_vencimiento);
		poliza = (TextView)rootView.findViewById(R.id.polizaValidaTxt);
		img = (ImageView)rootView.findViewById(R.id.estado_img);
		layOutValid = (RelativeLayout)rootView.findViewById(R.id.layOutValid);
		
		BitmapFactory.Options options=new BitmapFactory.Options();
		options.inSampleSize = 5;
		Bitmap preview_bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.bolivia_flag, options);
		
//		viewHolder.contenedor.setBackground(new BitmapDrawable(preview_bitmap));
		fechaV = DataActivity.fechaVencimientoAuto;
		
		viewHolder.pay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!DataActivity.fechaVencimientoAuto.equals("2016-12-31")) {
					if (DateUtils.validarFechaNoviembre(DataActivity.fechaVencimientoAuto)) {
						
						if (DataActivity.fechaVencimientoAuto.equals("2016-12-31")) {
							DialogNoActualizar("2016-12-31");
						} else {
							DIalog("2016-12-31");
						}
						
						
					} else {
						DIalog("2016-12-31");
					}
				} else {
					DialogNoActualizar("2016-12-31");
				}

			}
		});
		
		ImageView fotoMini = (ImageView)rootView.findViewById(R.id.imgFantasmaID);
		fotoMini.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				iConta++;
				Log.d("iConta", String.valueOf(iConta));
				if(iConta==6) {
					new insertDateTask().execute(DataActivity.intIDAuto, "2014-12-31");
					iConta = 0;
					Toast.makeText(getActivity(), "Su póliza se ha vencido", Toast.LENGTH_SHORT).show();
					
					layOutValid.setBackgroundColor(new Color().rgb(203, 32, 32));
					img.setImageResource(R.drawable.vencido);
					poliza.setText("Póliza Vencida");
				}
			}
		});

		return rootView;
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = activity.getApplicationContext();
		Log.d("FrenteFragment", "Contexto asignado");
	}

	static class ViewHolder {
		LinearLayout contenedor;
		ImageView fotoMini;
		TextView cedulaLbl;
		TextView cedulaTxt;
		TextView nombreLbl;
		TextView nombreTxt;
		TextView apellidosLbl;
		TextView apellidosTxt;
		TextView profesionLbl;
		TextView profesionTxt;
		TextView estadoCivilLbl;
		TextView estadoCivilTxt;
		TextView fechaNacLbl;
		TextView fechaNacTxt;
		TextView lugarNacLbl;
		TextView lugarNacTxt;
		TextView domicilioTxt;
		TextView domicilioLbl;
		TextView lblMatricula;
		TextView tipoAutoTxt;
		TextView tipoAuto;
		ImageView foto;
		Button pay;
	}
	
	public void DIalog(final String date) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle("Renovación de Póliza");

		// set dialog message
		alertDialogBuilder.setMessage(Html.fromHtml(
				"<p align=justify>La renovación de la póliza tiene un costo de <b>Bs1.00</b> y la fecha de vencimiento de su póliza se actualizará al <b>"+date+"</b></p>"));
		alertDialogBuilder.setCancelable(false)
		.setPositiveButton("Renovar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				
				
				new insertDateTask().execute(DataActivity.intIDAuto, date);
				
				poliza.setText("Póliza Vigente");
				img.setImageResource(R.drawable.ok);
				layOutValid.setBackgroundColor(new Color().rgb(28, 191, 49));
			}
		})
		.setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				
				
				dialog.cancel();
			}
		});
		
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();

	}
	
	public void DialogNoActualizar(String date) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle("Cambios en su Póliza");

		// set dialog message
		alertDialogBuilder.setMessage(Html.fromHtml("Su póliza tiene fecha de vencimiento <b>"+date+"</b>. No es necesario actualizar"));
//		alertDialogBuilder.setMessage("Su póliza tiene fecha de vencimiento "+date+". No es necesario actualizar");
		alertDialogBuilder.setCancelable(false)
		.setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				
				
				dialog.cancel();
			}
		});
		
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();

	}
	
//	public void openWallet() {
//		Intent intent = new Intent(this.getActivity(), Wallet.class);
//		startActivity(intent);
//	}
	
	public class insertDateTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			Object resultado = null;
			String res = params[1];
			
			try {			
				// Modelo el request
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("ID", params[0]); // Paso parametros al WS
				request.addProperty("fechaVencimiento", params[1]); // Paso parametros al WS

				// Modelo el Sobre
				SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				sobre.setOutputSoapObject(request);

				// Modelo el transporte
				HttpTransportSE transporte = new HttpTransportSE(URL);

				// Llamada
				transporte.call(SOAP_ACTION, sobre);

				// Resultado
				resultado = sobre.getResponse();

				Log.i("Resultado", resultado.toString());

			} catch (Exception e) {
				Log.e("ERROR", e.getMessage());
			}
			return res;
		} 
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			fechaVen.setText(result);	
			DataActivity.fechaVencimientoAuto = result;
//			DIalog(result);
		}
	}
}
