package c.neo.boa.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	
	public static String validarFechaPoliza(String fechaVencimiento) {
		Calendar c = Calendar.getInstance();
		String polizaValida = "";

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date_vencimiento = null;
		try {
			date_vencimiento = df.parse(fechaVencimiento);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(date_vencimiento); 

		Date hoy = c.getTime();

		if (hoy.before(date_vencimiento)) {
			polizaValida = "OK";
		} else {
			polizaValida = "VENCIDA";
		}
		return polizaValida;
	}
	
	public static Boolean validarFechaNoviembre(String fechaVencimiento) {
		boolean polizaValida = false;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date_vencimiento = null;
		try {
			date_vencimiento = df.parse(fechaVencimiento);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(date_vencimiento); 

		Date noviembre = null;
		try {
			noviembre = df.parse("2015-11-01");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (date_vencimiento.after(noviembre)) {
			polizaValida = true;
		} else {
			polizaValida = false;
		}
		return polizaValida;
	}

}
